﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class FootDataPaser
{
    Queue<byte> buffer;
    Queue<byte> data;
    public byte[] Data;

    bool head = false;
    ushort headCount = 0;
    public int count = 0;

    public FootDataPaser()
    {
        buffer = new Queue<byte>();
        data = new Queue<byte>();
    }

    public void Push(byte[] buffer,int offset,int count)
    {
        for (int i = 0; i < count; i++)
        {
            byte b = buffer[offset + i];

            if (!head)
            {
                switch (headCount)
                {
                    case 0:
                        if (b == 0x00) headCount++;
                        break;
                    case 1:
                        if (b == 0xFF) headCount++;
                        else headCount = 0;
                        break;
                    case 2:
                        if (b == 0xD2) head = true;
                        headCount = 0;
                        break;
                    default:
                        break;
                }
            }
            else
            {
                data.Enqueue(b);
                if (data.Count == 126)
                {
                    Data = data.ToArray();
                    data.Clear();
                }
            }
        }
    }

    public byte[] Paser()
    {
        while (buffer.Count > 0)
        {
            if (head)
            {
                data.Enqueue(buffer.Dequeue());
                if (data.Count == 115)
                {
                    head = false;
                    byte[] b = data.ToArray();
                    data.Clear();
                    return b;
                }
            }
            else
            {
                byte b = buffer.Dequeue();
                switch (headCount)
                {
                    case 0:
                        if (b == 0x00) headCount++;
                        break;
                    case 1:
                        if (b == 0xFF) headCount++;
                        else headCount = 0;
                        break;
                    case 2:
                        if (b == 0xD2) head = true;
                        headCount = 0;
                        break;
                    default:
                        break;
                }
            }
            System.Threading.Thread.SpinWait(1);
        }
        return null;
    }

    public void PaserAsync(DataPaserAsyncArgs e)
    {
        e.StartOperationCommon(this);
        e.StartOperationPaser();
    }

}
