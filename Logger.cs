﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;

public static class Logger
{
    public static event Action<LogMsg> OnLogNewMessage;
    private static List<LogMsg> logs = new List<LogMsg>();

    static Logger()
    {
        
    }

    public static void Log(string format,params object[] objs)
    {
        Log(string.Format(format, objs));
    }

    public static void Log(string msg)
    {
        Log(WarningLevel.Info, msg);
    }

    public static void Log(WarningLevel level,string format,params object[] objs)
    {
        Log(level, string.Format(format, objs));
    }

    public static void Log(WarningLevel level, string msg)
    {
        LogMsg log = new LogMsg(level, null, msg);
        logs.Add(log);
        OnLogNewMessage(log);
    }

    public struct LogMsg
    {
        public WarningLevel level;
        public DateTime time;
        public string msg;
        public LogMsg(WarningLevel? level, DateTime? time,string msg)
        {
            this.level = level ?? WarningLevel.Info;
            this.time = time ?? DateTime.Now;
            this.msg = msg;
        }

        public override string ToString()
        {
            return String.Format("[{0}] {1}\n", time.ToString("yy-MM-dd HH:mm:ss"), msg);
        }
    }

    [Flags]
    public enum WarningLevel : byte
    {
        Info = 0,
        Config = 1,
        Fine = 4,
        Finer = 5,
        Finest = 6,
        Warning = 8,
        Severe = 9,
    }

    public static string BytesToString(byte[] data,int baseNum = 16)
    {
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < data.Length; i++)
        {
            if (i != 0) sb.Append(' ');
            string str = Convert.ToString((int)data[i], baseNum);
            sb.Append(str.PadLeft(2,'0'));
        }

        return sb.ToString();
    }
}

