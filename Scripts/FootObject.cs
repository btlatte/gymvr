﻿using UnityEngine;
using System.Collections;

public class FootObject : MonoBehaviour
{
    static readonly int[] points = { 3, 4, 4, 4, 4, 4, 4, 4, 4, 4, 5, 5, 6, 6, 6, 6, 5, 4, 3 };

    public string DeviceName;
    public string FootName;
    public Transform parent;

    byte[] data;
    SpriteRenderer[,] srs;
    Color32[] colors;

    FootDataPaser paser = new FootDataPaser();
    SerialConnector connector = new SerialConnector();
    ConnectorAsyncArgs recieveArgs = new ConnectorAsyncArgs();
    DataPaserAsyncArgs paserArgs = new DataPaserAsyncArgs();

    byte[,] pointsPress = new byte[20, 6];
    string str = "";

    void Awake()
    {
        Input.gyro.enabled = true;

        colors = new Color32[255];
        for (int i = 0; i < colors.Length; i++)
        {
            colors[i] = new Color32(0xff, (byte)i, (byte)i, 0xff);
        }

        recieveArgs.Completed += ConnArgs_Completed;
        paserArgs.Completed += PaserArgs_Completed;

        InitSprite();
    }

    // Use this for initialization
    void Start()
    {
        connector.Init(DeviceName);
        connector.Connect();
        Send(MESSAGE.START_TRANS);
    }

    // Update is called once per frame
    void Update()
    {
        connector.ReceiveAsync(recieveArgs);
        //paser.PaserAsync(paserArgs);

        if (paser.Data != null)
        {
            //str = data.Length.ToString();
            //str = BytesToString(paser.Data);
//            StartCoroutine(FootDataVisualize());
        }

    }

    void OnGUI()
    {
        GUI.Label(new Rect(0, 0, Screen.width, Screen.height), "Text: " + str);
    }

    void InitSprite()
    {
        srs = new SpriteRenderer[19, 6];
        for (int x = 0; x < 19; x++)
        {
            for (int y = 0; y < points[x]; y++)
            {
                Transform trans = Instantiate<Transform>(parent);
                trans.parent = transform;
                trans.localPosition = new Vector3(x * 0.5f, y * 0.5f, 0);
                srs[x, y] = trans.GetComponent<SpriteRenderer>();
            }
        }
    }

    IEnumerator FootDataVisualize()
    {
        int i = 0;
        for (int y = 0; y < 6; y++)
        {
            for (int x = 0; x < 19; x++)
            {
                i++;

                if (y < points[x])
                {
                    pointsPress[x, y] = data[i];
                    byte c = (byte)(0xFF - pointsPress[x, y]);
                    try
                    {
                        //srs[x, y].color = colors[c];
                    }
                    catch (System.IndexOutOfRangeException)
                    {
                        Debug.Log(string.Format("{0},{1}  {2},{3}", x, y, srs.GetLength(0), srs.GetLength(1)));
                        throw;
                    }

                    //str = c.ToString();
                }
            }
        }
        yield return null;
    }

    private void ConnArgs_Completed(object sender, ConnectorAsyncArgs e)
    {
        str += BytesToString(e.Buffer);

        paser.Push(e.Buffer, 0, e.Buffer.Length);
    }


    private void PaserArgs_Completed(object sender, DataPaserAsyncArgs e)
    {
        if (e.Data != null)
        {
            data = e.Data;
            str = string.Format("{0}:{1}",paser.count, BytesToString(e.Data));
        }
    }

    void Send(MESSAGE msg, ushort value = 0)
    {
        byte[] b = new byte[] { 0x00, 0x00, 0x00, 0xFF };
        b[1] = (byte)msg;
        if ((byte)msg <= 0xA2) b[2] = System.Convert.ToByte(value);

        connector.Send(b, 0, b.Length);
    }

    public enum MESSAGE : byte
    {
        START_TRANS = 0xA2,
        STOP_TRANS = 0xA0,
        CHANGE_SPEED = 0xA6
    }

    public static string BytesToString(byte[] data, int baseNum = 16)
    {
        System.Text.StringBuilder sb = new System.Text.StringBuilder();

        for (int i = 0; i < data.Length; i++)
        {
            if (i != 0) sb.Append(' ');
            string str = System.Convert.ToString((int)data[i], baseNum);
            sb.Append(str.PadLeft(2, '0'));
        }

        return sb.ToString();
    }

    public void OnDestroy()
    {
        connector.Close();
    }
}
