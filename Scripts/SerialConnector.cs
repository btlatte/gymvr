﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO.Ports;
using System.Threading;

public class SerialConnector {

    public SerialPort port;
    Thread thread;

    public EventHandler<ConnectorAsyncArgs> a;
    public int RevieveBufferSize
    {
        get
        {
            return port.ReadBufferSize;
        }
    }

    public SerialConnector()
    {
        
    }

    public void Init(object o)
    {
        if (o is string) port = new SerialPort(@"\\.\" + o as string, 115200, Parity.None, 8, StopBits.One);
        port.ReadBufferSize = 256;
        port.ReadTimeout = 1;
    }

    public void Connect()
    {
        port.Open();

        port.DiscardInBuffer();
        port.DiscardOutBuffer();
    }

    public void Receive(byte[] buffer,int offset,int count)
    {
        port.Read(buffer, offset, count);
    }

    public void ReceiveAsync(ConnectorAsyncArgs e)
    {
        e.StartOperationCommon(this);
        e.StartOperationReceive();
    }

    public void Send(byte[] buffer,int offset,int count)
    {
        port.Write(buffer, offset, count);
    }

    internal void Close()
    {
        port.Close();
    }
}
