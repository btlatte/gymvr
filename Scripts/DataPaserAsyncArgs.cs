﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

public class DataPaserAsyncArgs : EventArgs
{
    public event EventHandler<DataPaserAsyncArgs> Completed;

    FootDataPaser m_Paser;
    byte[] m_data;
    public byte[] Data
    {
        get
        {
            return m_data;
        }
    }

    bool OnPaser = false;

    public DataPaserAsyncArgs()
    {

    }

    internal void StartOperationCommon(FootDataPaser paser)
    {
        m_Paser = paser;
    }

    internal void StartOperationPaser()
    {
        ThreadPool.QueueUserWorkItem((o) =>
        {
            if (!OnPaser)
            {
                OnPaser = true;

                m_data = m_Paser.Paser();
                Completed(m_Paser, this);

                OnPaser = false;
            }
        });
    }
}