﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

public class ConnectorAsyncArgs : EventArgs, IDisposable
{
    public event EventHandler<ConnectorAsyncArgs> Completed;

    SerialConnector _connector;
    byte[] m_Buffer;
    public byte[] Buffer
    {
        get
        {
            return m_Buffer;
        }
    }

    bool OnRecieve = false;

    public void Dispose()
    {
        throw new NotImplementedException();
    }

    public void SetBuffer(byte[] buffer, int offset, int count)
    {
        
    }

    protected virtual void OnCompleted(ConnectorAsyncArgs e)
    {
        EventHandler<ConnectorAsyncArgs> completed = this.Completed;

        if(completed != null)
        {
            completed(e._connector, e);
        }
    }

    internal void StartOperationReceive()
    {
        ThreadPool.QueueUserWorkItem((o) =>
        {
            if(!OnRecieve)
            {
                OnRecieve = true;

                if (m_Buffer == null) m_Buffer = new byte[_connector.RevieveBufferSize];
                _connector.Receive(m_Buffer, 0, m_Buffer.Length);
                Completed(_connector, this);

                OnRecieve = false;
            }
        });
    }


    internal void StartOperationCommon(SerialConnector connector)
    {
        _connector = connector;
    }
}
