﻿using System.Text;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine.UI;

public class FootCom : MonoBehaviour {
    /*public string DeviceName;
    public string FootName;
    public Transform parent;
    public Text text;
    public AnimationCurve curve;
    public Button button;
    Stream stream;
    StreamWriter sw;
    string fname = "";

    BluetoothDevice device;
    SpriteRenderer[,] srs;
    static int[] points = { 3,4,4,4,4,4,4,4,4,4,5,5,6,6,6,6,5,4,3};
    bool writing = false;
    string bufferStr = "";
    string data = "";

    List<byte> buffer;
    byte[,] pointsPress;

    void Awake()
    {
        pointsPress = new byte[19, 6];
        buffer = new List<byte>(450);
        //device = new BluetoothDevice();
        //device.Name = DeviceName;
        button.onClick.AddListener(() => Button_OnClick());
        //stream = File.Open(Application.persistentDataPath + "", FileMode.OpenOrCreate, FileAccess.Write, FileShare.Read);
        InitSprite();
    }

    void InitEvent()
    {
        device.ReadingCoroutine = ReadCoroutine;
        device.OnConnected += Device_OnConnected;
        device.OnSendingError += Device_OnSendingError;
        device.OnReadingError += Device_OnReadingError;
        device.OnDeviceNotFound += Device_OnDeviceNotFound;
        device.OnDeviceOFF += Device_OnDeviceOFF;
        device.OnDisconnected += Device_OnDisconnected;
    }

    void Button_OnClick()
    {
        BluetoothAdapter.OnDevicePicked += BluetoothAdapter_OnDevicePicked;
        text.text += "Select Device\n";
        BluetoothAdapter.showDevices();
    }

    private void BluetoothAdapter_OnDevicePicked(BluetoothDevice obj)
    {
        text.text += "Selected\n";
        BluetoothAdapter.OnDevicePicked -= BluetoothAdapter_OnDevicePicked;
        this.device = obj;
        this.device.setEndByte(0x32);
        InitEvent();
        InitSave();
        this.device.connect();
    }

    private void Device_OnDisconnected(BluetoothDevice obj)
    {
        text.text += "Device Disconnected\n";
    }

    private void Device_OnDeviceOFF(BluetoothDevice obj)
    {
        text.text += "Device OFF\n";
    }

    private void Device_OnDeviceNotFound(BluetoothDevice obj)
    {
        text.text += "Not Found\n";
    }

    private void Device_OnReadingError(BluetoothDevice obj)
    {
        text.text += "Reading Error\n";
    }

    private void Device_OnSendingError(BluetoothDevice device)
    {
        text.text += "Sending Error\n";
    }

    // Use this for initialization
    void Start () {
        text.text += "Inited" + "\n";
        BluetoothAdapter.askEnableBluetooth();
        text.text += BluetoothAdapter.isBluetoothEnabled().ToString() + "\n";
        text.text += SystemInfo.supportsGyroscope.ToString() + "\n";
        Input.gyro.enabled = true;  
        //device.connect(50,100,true);
	}

    void Device_OnConnected(BluetoothDevice device)
    {
        conn = true;
        text.text += "Connected" + "\n";
        try
        {
            device.send(new byte[] { 0x00, 0xA2, 0x00, 0xff, 0x32 });
            text.text += "Sended\n";
        }
        catch (System.Exception e)
        {
            text.text += e.ToString() + "\n";
        }
        
    }

  
    
    void SendStart()
    {
        try
        {
            device.send(new byte[] { 0x00, 0xA2, 0x00, 0xff , 0x32 });
            text.text += "Sended\n";
        }
        catch (System.Exception e)
        {
            text.text += e.ToString() + "\n";
        }
    }
    bool conn = false;
    // Update is called once per frame
    void Update () {
        if(conn) SendStart();
        if (data != "")
        {
           // text.text = data;
            bufferStr = data;
            data = "";
            StartCoroutine(Depack());
        }
    }

    void InitSprite()
    {
        srs = new SpriteRenderer[19,6];
        for (int x = 0; x < 19; x++)
        {
            for (int y = 0; y < points[x]; y++)
            {
                Transform trans = Instantiate<Transform>(parent);
                trans.parent = transform;
                trans.localPosition = new Vector3(x * 0.5f, y * 0.5f, 0);
                srs[x,y] = trans.GetComponent<SpriteRenderer>();
            }
        }
    }

    IEnumerator ReadCoroutine(BluetoothDevice device)
    {
        while (device.IsReading)
        {
            if (device.IsDataAvailable)
            {
                byte[] recv = device.read();
                if (recv.Length > 0)
                {
                    text.text += "#";
                    AddBuffer(recv);
                }
            }
            yield return null;
        }
    }

    void InitSave()
    {
        if (stream != null) stream = null;
        if (sw != null) sw = null;
        string filename = string.Format("{0}_{1}.csv",System.DateTime.Now.ToString("yyyyMMdd_HHmmss"),FootName);
        string path = Application.persistentDataPath;
        path = path.Substring(0, path.LastIndexOf('/'));
        path = Path.Combine(path, filename);
        fname = path;
        stream = File.Open(path, FileMode.OpenOrCreate, FileAccess.Write, FileShare.Read);
        sw = new StreamWriter(stream);
    }

    void SaveData()
    {
        byte[,] b = new byte[19, 6];
        pointsPress.CopyTo(b, 0);

        for (int x = 0; x < 19; x++)
        {
            for (int y = 0; y < points[x]; y++)
            {
                sw.Write(b[x, y]);
                if(x != 18 && y!= points[18] - 1)sw.Write(",");
            }
        }

        sw.WriteLine();
    }

    bool header = false;
   
    int idx = 0;
    void AddBuffer(byte[] b)
    {
        for (int i = 0; i < b.Length; i++)
        {
            if (header)
            {
                if (writing)
                {
                    if (b[i] == '\n')
                    {
                        if (idx++ > 5)
                        {
                            data = Encoding.ASCII.GetString(buffer.ToArray());
                            writing = header = false;
                            idx = 0;
                            buffer.Clear();
                        }
                        else
                        {
                            buffer.Add(b[i]);
                        }
                    }
                    else
                        buffer.Add(b[i]);
                }
                else
                {
                    if (b[i] == '-')
                    {
                        writing = true;
                    }
                }
                
            }
            else
            {
                if (b[i] == '-') header = true;
            }
            
        }
    }

    IEnumerator Depack()
    {
        string s = bufferStr;
        StringReader reader = new StringReader(s);
        reader.ReadLine();

        for (int i = 0; i < 6; i++)
        {
            string[] list = reader.ReadLine().Substring(3).Split(' ');
            for (int x = 0; x < 19; x++)
            {
                pointsPress[x, i] = System.Convert.ToByte(list[x],16);
            }
        }

        //SaveData();

        for (int x = 0; x < 19; x++)
        {
            for (int y = 0; y < points[x]; y++)
            {
                byte c = (byte)(0xFF - pointsPress[x,y]);
                srs[x, y].color = new Color32(0xFF, c, c, 0xFF);
                sw.Write(pointsPress[x, y]);
                if (x != 18 && y != points[18] - 1) sw.Write(",");
            }
        }
        sw.WriteLine();

        text.text = string.Format("Running\nSaveing to {0}",fname);

        yield return null;
    }*/
}
