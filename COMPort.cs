﻿using System;
using System.Collections;
using System.IO.Ports;
using System.Threading;

public class COMPort : SerialPort{
    Thread thread;
    public static readonly byte[] START_TRANS = new byte[] { 0x00, 0xA2, 0x00, 0xFF };
    public static readonly byte[] STOP_TRANS = new byte[] { 0x00, 0xA0, 0x00, 0xFF };
    public static readonly byte[] OPEN_PRESS = new byte[] { 0x00, 0xA2, 0x00, 0xFF };
    public static readonly byte[] STOP_PRESS = new byte[] { 0x00, 0xA2, 0x00, 0xFF };
    public string StringData = "";
    public byte[,] buffer = new byte[6, 19];
    public byte[,] data = new byte[6, 19];

    public bool Running = false;
    int i = 0;

    public enum COMMAND
    {
        START_TRANS,
        STOP_TRANS
    }

    public COMPort(string portName) : base(@"\\.\" + portName, 115200, Parity.None, 8, StopBits.One)
    {
        Handshake = Handshake.None;
        RtsEnable = true;
        DtrEnable = true;
        WriteTimeout = 1000;
        ReadTimeout = 1000;
        DataReceived += new SerialDataReceivedEventHandler(COMPort_DataReceived);
        thread = new Thread(Run);
    }

    private void COMPort_DataReceived(object sender, SerialDataReceivedEventArgs e)
    {
       // StringData = "AAA";
        //UnityEngine.Debug.Log("A");
        //SerialPort sp = (SerialPort) sender;
        //StringData += string.Format("{0}\n", sp.ReadExisting());
    }

    public void Start()
    {
        Running = true;
        thread.Start();
    }

    public void Stop()
    {
        Running = false;
        thread.Abort();
    }

    void Run()
    {
        while (Running)
        {
            //StringData = (i++).ToString();
            try
            {
                if (ReadByte() > 0)
                {
                    if (ReadByte() == '-')
                    {
                        StringData = "";
                        ReadLine();
                        i = 0;
                        Array.Copy(buffer, data, buffer.Length);
                    }
                    else
                    {
                        ReadByte();
                        string str = ReadLine();
                        string[] value = str.Split(' ');
                        for (int j = 0; j < 19; j++)
                        {
                            buffer[i, j] = Convert.ToByte(value[j], 16);
                        }
                        i++;
                    }
                    
                }
                
            }
            catch (Exception e)
            {
                //UnityEngine.Debug.Log(e.ToString());
            }
        }
    }

    public void Command(COMMAND cmd)
    {
        switch (cmd)
        {
            case COMMAND.START_TRANS:
                Send(START_TRANS);
                break;
            case COMMAND.STOP_TRANS:
                Send(STOP_TRANS);
                break;
            default:
                break;
        }
    }

    private void Send(byte[] buf)
    {
        this.Write(buf, 0, buf.Length);
    }

    /*public new string ReadLine()
    {
        
    }*/
}
